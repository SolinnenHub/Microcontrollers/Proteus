#pragma GCC push_options
#pragma GCC optimize ("Os")

#include <core.h>
#include <cpu.h>

#pragma GCC pop_options

CPU &cpu = Cpu;

void peripheral_setup () {
}

void peripheral_loop() {
}

long int t0;
long int t1;
long int delta;

const int greenPinOutput  = 0;
const int yellowPinOutput = 1;
const int redPinOutput    = 2;
const int debugPinOutput  = 7;

const int greenPinInput  = 3;
const int redPinInput    = 4;

bool inputGreen   = false;
bool inputRed     = false;
bool inverted     = false;

// 0 = red; 1 = yellow; 2 = green;
int currentColor;

int greenDelay  = 4000;
int yellowDelay = 1000;
int redDelay    = 5000;
int shortDelay = yellowDelay / 3;

void setup() {
  pinMode(greenPinOutput,  OUTPUT);
  pinMode(yellowPinOutput, OUTPUT);
  pinMode(redPinOutput,    OUTPUT);
  pinMode(debugPinOutput,  OUTPUT);

  pinMode(greenPinInput,  INPUT);
  pinMode(redPinInput,    INPUT);

  t0 = millis();

  currentColor = 0;

  digitalWrite(greenPinOutput,  LOW);
  digitalWrite(yellowPinOutput, LOW);
  digitalWrite(redPinOutput,    HIGH);
}

void loop() {
  inputGreen = digitalRead(greenPinInput);
  inputRed = digitalRead(redPinInput);

  t1 = millis();
  delta = t1 - t0;

  // DEBUG
  if (inputGreen || inputRed) {
    digitalWrite(debugPinOutput, HIGH);
  } else {
    digitalWrite(debugPinOutput, LOW);
  }

  if (inputGreen || inputRed) {
    if (inputRed && !inputGreen) { // SET RED
      if (currentColor == 0) {
        t0 = t1;
      } else if (currentColor == 1) {
        inverted = false;
        if (delta > shortDelay) {
          t0 -= yellowDelay;
        }
      } else {
        digitalWrite(yellowPinOutput, HIGH);
        digitalWrite(greenPinOutput,  LOW);
        delay(shortDelay);
        digitalWrite(redPinOutput,    HIGH);
        digitalWrite(yellowPinOutput, LOW);
        currentColor = 0;
        t0 = t1;
      }
    } else if (inputGreen && !inputRed) { // SET GREEN
      if (currentColor == 2) {
        t0 = t1;
      } else if (currentColor == 1) {
        inverted = true;
        if (delta > shortDelay) {
          t0 -= yellowDelay;
        }
      } else {
        digitalWrite(redPinOutput,    LOW);
        digitalWrite(yellowPinOutput, HIGH);
        delay(shortDelay);
        digitalWrite(yellowPinOutput, LOW);
        digitalWrite(greenPinOutput,  HIGH);
        currentColor = 2;
        t0 = t1;
      }
    } else { // SET YELLOW
      digitalWrite(redPinOutput,    LOW);
      digitalWrite(yellowPinOutput, HIGH);
      digitalWrite(greenPinOutput,  LOW);
      currentColor = 1;
      inverted = true;
      t0 = t1;
    }
  }

  if (currentColor == 0 && delta > redDelay) {
    digitalWrite(redPinOutput,    LOW);
    digitalWrite(yellowPinOutput, HIGH);
    digitalWrite(greenPinOutput,  LOW);
    currentColor = 1;
    t0 = t1;
  } else if (currentColor == 1 && delta > yellowDelay) {
    digitalWrite(yellowPinOutput, LOW);
    if (inverted) {
      digitalWrite(redPinOutput,    HIGH);
      digitalWrite(greenPinOutput,  LOW);
      currentColor = 0;
      inverted = false;
    } else {
      digitalWrite(redPinOutput,    LOW);
      digitalWrite(greenPinOutput,  HIGH);
      currentColor = 2;
    }
    t0 = t1;
  } else if (currentColor == 2 && delta > greenDelay) {
    digitalWrite(redPinOutput,    LOW);
    digitalWrite(yellowPinOutput, HIGH);
    digitalWrite(greenPinOutput,  LOW);
    currentColor = 1;
    t0 = t1;
    inverted = true;
  }
}