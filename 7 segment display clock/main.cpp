#pragma GCC push_options
#pragma GCC optimize ("Os")

#include <core.h>
#include <cpu.h>

#pragma GCC pop_options

CPU &cpu = Cpu;

// button definition
#define buttonManual A0
#define buttonSave   A1
#define buttonAdd    A2
#define buttonSwitch A3
 
// segment pin definitions
#define SegA   12
#define SegB   11
#define SegC   10
#define SegD    9
#define SegE    8
#define SegF    7
#define SegG    6
// common pins of the four digits definitions
#define Dig1    5
#define Dig2    4
#define Dig3    3
#define Dig4    2
 
void setup() {
  pinMode(buttonManual, INPUT_PULLUP);
  pinMode(buttonSave,   INPUT_PULLUP);
  pinMode(buttonAdd,    INPUT_PULLUP);
  pinMode(buttonSwitch, INPUT_PULLUP);
  pinMode(SegA, OUTPUT);
  pinMode(SegB, OUTPUT);
  pinMode(SegC, OUTPUT);
  pinMode(SegD, OUTPUT);
  pinMode(SegE, OUTPUT);
  pinMode(SegF, OUTPUT);
  pinMode(SegG, OUTPUT);
  pinMode(Dig1, OUTPUT);
  pinMode(Dig2, OUTPUT);
  pinMode(Dig3, OUTPUT);
  pinMode(Dig4, OUTPUT);

  pinMode(0, OUTPUT); // DEBUG
  
  digitalWrite(Dig1, LOW);
  digitalWrite(Dig2, LOW);
  digitalWrite(Dig3, LOW);
  digitalWrite(Dig4, LOW);
 
  // Timer1 module overflow interrupt configuration
  TCCR1A = 0;
  TCCR1B = 1;  // enable Timer1 with prescaler = 1 ( 16 ticks each 1 µs)
  TCNT1  = 0;  // set Timer1 preload value to 0 (reset)
  TIMSK1 = 1;  // enable Timer1 overflow interrupt
}
 
unsigned long long clock = 0;
int little_shift = 0;
int big_shift = 0;
byte current_digit = 0;
byte changing_big = 0;
byte is_manual = 0;

ISR(TIMER1_OVF_vect) {
  digitalWrite(Dig1, LOW);
  digitalWrite(Dig2, LOW);
  digitalWrite(Dig3, LOW);
  digitalWrite(Dig4, LOW);

  if (is_manual) {
    if (changing_big == 1) {
      if (current_digit == 1) {
        disp(((clock % (60*60)) / 60 % 24) / 10); 
        digitalWrite(Dig1, HIGH);
      } else if (current_digit == 2) {
        disp((((clock % (60*60)) / 60) % 24) % 10);
        digitalWrite(Dig2, HIGH);
      }
    } else {
      if (current_digit == 3) {
        disp((clock % 60) / 10);
        digitalWrite(Dig3, HIGH);
      } else if (current_digit == 4) {
        disp((clock % 60) % 10);
        digitalWrite(Dig4, HIGH);
      }
    }
  } else {
    if (current_digit == 1) {
       disp(((clock % (60*60)) / 60 % 24) / 10); 
       digitalWrite(Dig1, HIGH);
    } else if (current_digit == 2) {
       disp((((clock % (60*60)) / 60) % 24) % 10);
       digitalWrite(Dig2, HIGH);
    } else if (current_digit == 3) {
       disp((clock % 60) / 10);
       digitalWrite(Dig3, HIGH);
    } else if (current_digit == 4) {
       disp((clock % 60) % 10);
       digitalWrite(Dig4, HIGH);
    }
  }
  current_digit = (current_digit % 4) + 1;
}

byte addButtonPressed = 0;
byte switchButtonPressed = 0;

void loop() {
  if (is_manual) {
    if (digitalRead(buttonAdd) == 0) {
      if (addButtonPressed == 0) {
        addButtonPressed = 1;
        if (changing_big == 1) {
          big_shift++;
        } else {
          little_shift++;
        }
      }
    } else {
      addButtonPressed = 0;
    }

    if (digitalRead(buttonSwitch) == 0) {
      if (switchButtonPressed == 0) {
        switchButtonPressed = 1;
        changing_big ^= 1;
      }
    } else {
      switchButtonPressed = 0;
    }
  }

  if (digitalRead(buttonManual) == 0) {
    if (is_manual == 0) {
      is_manual = 1;
      changing_big = 0;
    }
  } else if (digitalRead(buttonSave) == 0) {
    if (is_manual == 1) {
      is_manual = 0;
    }
  }

  clock = (millis() / 1000 + little_shift + 60*big_shift) % (60*60*24);
}
 
void disp(byte number) {
  switch (number) {
    case 0:
      digitalWrite(SegA, LOW);
      digitalWrite(SegB, LOW);
      digitalWrite(SegC, LOW);
      digitalWrite(SegD, LOW);
      digitalWrite(SegE, LOW);
      digitalWrite(SegF, LOW);
      digitalWrite(SegG, HIGH);
      break;
 
    case 1:
      digitalWrite(SegA, HIGH);
      digitalWrite(SegB, LOW);
      digitalWrite(SegC, LOW);
      digitalWrite(SegD, HIGH);
      digitalWrite(SegE, HIGH);
      digitalWrite(SegF, HIGH);
      digitalWrite(SegG, HIGH);
      break;
 
    case 2:
      digitalWrite(SegA, LOW);
      digitalWrite(SegB, LOW);
      digitalWrite(SegC, HIGH);
      digitalWrite(SegD, LOW);
      digitalWrite(SegE, LOW);
      digitalWrite(SegF, HIGH);
      digitalWrite(SegG, LOW);
      break;
 
    case 3:
      digitalWrite(SegA, LOW);
      digitalWrite(SegB, LOW);
      digitalWrite(SegC, LOW);
      digitalWrite(SegD, LOW);
      digitalWrite(SegE, HIGH);
      digitalWrite(SegF, HIGH);
      digitalWrite(SegG, LOW);
      break;
 
    case 4:
      digitalWrite(SegA, HIGH);
      digitalWrite(SegB, LOW);
      digitalWrite(SegC, LOW);
      digitalWrite(SegD, HIGH);
      digitalWrite(SegE, HIGH);
      digitalWrite(SegF, LOW);
      digitalWrite(SegG, LOW);
      break;
 
    case 5:
      digitalWrite(SegA, LOW);
      digitalWrite(SegB, HIGH);
      digitalWrite(SegC, LOW);
      digitalWrite(SegD, LOW);
      digitalWrite(SegE, HIGH);
      digitalWrite(SegF, LOW);
      digitalWrite(SegG, LOW);
      break;
 
    case 6:
      digitalWrite(SegA, LOW);
      digitalWrite(SegB, HIGH);
      digitalWrite(SegC, LOW);
      digitalWrite(SegD, LOW);
      digitalWrite(SegE, LOW);
      digitalWrite(SegF, LOW);
      digitalWrite(SegG, LOW);
      break;
    
    case 7:
      digitalWrite(SegA, LOW);
      digitalWrite(SegB, LOW);
      digitalWrite(SegC, LOW);
      digitalWrite(SegD, HIGH);
      digitalWrite(SegE, HIGH);
      digitalWrite(SegF, HIGH);
      digitalWrite(SegG, HIGH);
      break;
 
    case 8:
      digitalWrite(SegA, LOW);
      digitalWrite(SegB, LOW);
      digitalWrite(SegC, LOW);
      digitalWrite(SegD, LOW);
      digitalWrite(SegE, LOW);
      digitalWrite(SegF, LOW);
      digitalWrite(SegG, LOW);
      break;
 
    case 9:
      digitalWrite(SegA, LOW);
      digitalWrite(SegB, LOW);
      digitalWrite(SegC, LOW);
      digitalWrite(SegD, LOW);
      digitalWrite(SegE, HIGH);
      digitalWrite(SegF, LOW);
      digitalWrite(SegG, LOW);
  }
}